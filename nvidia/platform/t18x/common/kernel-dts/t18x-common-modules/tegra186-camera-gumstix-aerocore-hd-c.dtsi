/*
 * Copyright (c) 2015-2017, NVIDIA CORPORATION.  All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/ {
	host1x {
		vi@15700000 {
			num-channels = <1>;
			ports {
				#address-cells = <1>;
				#size-cells = <0>;
				port@0 {
					reg = <0>;
					aerocore_vi_in0: endpoint {
						csi-port = <2>;
						bus-width = <2>;
						remote-endpoint = <&aerocore_csi_out0>;
					};
				};
			};
		};

		nvcsi@150c0000 {
			num-channels = <1>;
			#address-cells = <1>;
			#size-cells = <0>;
			channel@0 {
				reg = <0>;
				ports {
					#address-cells = <1>;
					#size-cells = <0>;
					port@0 {
						reg = <0>;
						aerocore_csi_in0: endpoint@0 {
							csi-port = <2>;
							bus-width = <2>;
							remote-endpoint = <&aerocore_ov5670_out0>;
						};
					};
					port@1 {
						reg = <1>;
						aerocore_csi_out0: endpoint@1 {
							remote-endpoint = <&aerocore_vi_in0>;
						};
					};
				};
			};
		};
	};

	i2c@3180000 {
                tca9548@70 {
                        i2c@3 {
				ov5670_c@36 {
				        status = "okay";
				        compatible = "nvidia,ov5670";
				        /* I2C device address */
				        reg = <0x36>;

				        clocks = <&tegra_car TEGRA186_CLK_EXTPERIPH2>,
				                         <&tegra_car TEGRA186_CLK_PLLP_OUT0>;
				        clock-names = "extperiph2", "pllp_grtba";
				        mclk = "extperiph2";
				        clock-frequency = <24000000>;
				        reset-gpios = <&tca6416_20 10 GPIO_ACTIVE_HIGH>;
				        pwdn-gpios = <&tca6416_20 11 GPIO_ACTIVE_HIGH>;
				        vana-supply = <&en_avdd_cam3>;
				        vdig-supply = <&en_dvdd_cam3>;

				        /* V4L2 device node location */
				        devnode = "video0";

				        /* Physical dimensions of sensor */
				        physical_w = "3.674";
				        physical_h = "2.738";

				        /* Define any required hw resources needed by driver */
				        /* ie. clocks, io pins, power sources */
				        avdd-reg = "vana";
				        dvdd-reg = "vdig";

				        /* Sensor output flip settings */
				        vertical-flip = "false";

				        mode0 { // OV5670_MODE_2592X1944
				                mclk_khz = "24000";
				                num_lanes = "2";
				                tegra_sinterface = "serial_c";
				                discontinuous_clk = "no";
				                dpcm_enable = "false";
				                cil_settletime = "0";

				                active_w = "2592";
				                active_h = "1944";
				                embedded_metadata_height = "1";
				                pixel_t = "bayer_gbrg";
				                readout_orientation = "0";
				                line_length = "2688";
				                inherent_gain = "1";
				                mclk_multiplier = "10";
				                pix_clk_hz = "240000000";

				                min_gain_val = "1.0";
				                max_gain_val = "16";
				                min_hdr_ratio = "1";
				                max_hdr_ratio = "64";
				                min_framerate = "1.816577";
				                max_framerate = "30";
				                min_exp_time = "34";
				                max_exp_time = "550385";
				        };
					mode1 { //OV5670_MODE_2560X1440
						mclk_khz = "24000";
						num_lanes = "2";
						tegra_sinterface = "serial_c";
						discontinuous_clk = "no";
						dpcm_enable = "false";
						cil_settletime = "0";

						active_w = "2560";
						active_h = "1440";
						embedded_metadata_height = "1";
						pixel_t = "bayer_gbrg";
						readout_orientation = "0";
						line_length = "2688";
						inherent_gain = "1";
						mclk_multiplier = "10";
						pix_clk_hz = "240000000";

						min_gain_val = "1.0";
						max_gain_val = "16";
						min_hdr_ratio = "1";
						max_hdr_ratio = "64";
						min_framerate = "2.787078";
						max_framerate = "30";
						min_exp_time = "22";
						max_exp_time = "358733";
					};
					mode2 { //OV5670_MODE_1920X1080
						mclk_khz = "24000";
						num_lanes = "2";
						tegra_sinterface = "serial_c";
						discontinuous_clk = "no";
						dpcm_enable = "false";
						cil_settletime = "0";

						active_w = "1920";
						active_h = "1080";
						embedded_metadata_height = "1";
						pixel_t = "bayer_gbrg";
						readout_orientation = "0";
						line_length = "2020";
						inherent_gain = "1";
						mclk_multiplier = "10";
						pix_clk_hz = "240000000";

						min_gain_val = "1.0";
						max_gain_val = "16";
						min_hdr_ratio = "1";
						max_hdr_ratio = "64";
						min_framerate = "2.787078";
						max_framerate = "60";
						min_exp_time = "22";
						max_exp_time = "358733";
					};

				        ports {
				                status = "okay";
				                #address-cells = <1>;
				                #size-cells = <0>;

				                port@0 {
				                        status = "okay";
				                        reg = <0>;
				                        aerocore_ov5670_out0: endpoint {
				                                status = "okay";
				                                csi-port = <2>;
				                                bus-width = <2>;
				                                remote-endpoint = <&aerocore_csi_in0>;
				                        };
				                };
				        };
				};
			};
		};
	};

	aerocore_lens@P5V27C {
		min_focus_distance = "0.0";
		hyper_focal = "0.0";
		focal_length = "2.67";
		f_number = "2.0";
		aperture = "2.0";
	};

	tegra-camera-platform {
		compatible = "nvidia, tegra-camera-platform";
		/**
		* Physical settings to calculate max ISO BW
		*
		* num_csi_lanes = <>;
		* Total number of CSI lanes when all cameras are active
		*
		* max_lane_speed = <>;
		* Max lane speed in Kbit/s
		*
		* min_bits_per_pixel = <>;
		* Min bits per pixel
		*
		* vi_peak_byte_per_pixel = <>;
		* Max byte per pixel for the VI ISO case
		*
		* vi_bw_margin_pct = <>;
		* Vi bandwidth margin in percentage
		*
		* max_pixel_rate = <>;
		* Max pixel rate in Kpixel/s for the ISP ISO case
		*
		* isp_peak_byte_per_pixel = <>;
		* Max byte per pixel for the ISP ISO case
		*
		* isp_bw_margin_pct = <>;
		* Isp bandwidth margin in percentage
		*/
		num_csi_lanes = <2>;
		max_lane_speed = <1500000>;
		min_bits_per_pixel = <10>;
		vi_peak_byte_per_pixel = <2>;
		vi_bw_margin_pct = <25>;
		max_pixel_rate = <160000>;
		isp_peak_byte_per_pixel = <5>;
		isp_bw_margin_pct = <25>;

		/**
		* The general guideline for naming badge_info contains 3 parts, and is as follows,
		* The first part is the camera_board_id for the module; if the module is in a FFD
		* platform, then use the platform name for this part.
		* The second part contains the position of the module, ex. “rear” or “front”.
		* The third part contains the last 6 characters of a part number which is found
		* in the module's specsheet from the vender.
		*/
		modules {
			module0 {
				badge = "aerocore_left_P5V27C";
				position = "rear";
				orientation = "1";
				drivernode0 {
					/* Declare PCL support driver (classically known as guid)  */
					pcl_id = "v4l2_sensor";
					/* Driver v4l2 device name */
					devname = "ov5670 33-0036";
					/* Declare the device-tree hierarchy to driver instance */
					proc-device-tree = "/proc/device-tree/i2c@3180000/tca9548@70/i2c@3/ov5670_c@36";
				};
				drivernode1 {
					/* Declare PCL support driver (classically known as guid)  */
					pcl_id = "v4l2_lens";
					proc-device-tree = "/proc/device-tree/aerocore_lens@P5V27C/";
				};
			};
		};
	};
};
