/*
 * Copyright (c) 2015-2016, NVIDIA CORPORATION.  All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <t18x-common-modules/tegra186-camera-gumstix-aerocore-hd.dtsi>
#include "dt-bindings/clock/tegra186-clock.h"

#define CAM0_RST_L	TEGRA_MAIN_GPIO(R, 5)
#define CAM0_PWDN	TEGRA_MAIN_GPIO(R, 0)

/* camera control gpio definitions */

/ {
        fixed-regulators {
		en_dvdd_cam4: regulator@cam207 {
			compatible = "regulator-fixed-sync";
			reg = <207>;
			regulator-name = "en-dvdd-cam4";
			regulator-min-microvolt = <1200000>;
			regulator-max-microvolt = <1200000>;
			gpio = <&tca6416_20 13 0>;
			enable-active-high;
		};
		en_avdd_cam4: regulator@cam208 {
			compatible = "regulator-fixed-sync";
			reg = <208>;
			regulator-name = "en-avdd-cam4";
			regulator-min-microvolt = <2800000>;
			regulator-max-microvolt = <2800000>;
			gpio = <&tca6416_20 12 0>;
			enable-active-high;
		};
	};

	i2c@3180000 {
                tca9548_70: tca9548@70 {
                        compatible = "nxp,pca9548";
                        reg = <0x70>;
                        #address-cells = <1>;
                        #size-cells = <0>;
                        vcc-supply = <&battery_reg>;
                        skip_mux_detect;
                        force_bus_start = <CAMERA_I2C_MUX_BUS(0)>;
                        i2c@0 {
                                reg = <0>;
                                i2c-mux,deselect-on-exit;
                                #address-cells = <1>;
                                #size-cells = <0>;
                        };
                        i2c@4 {
                                reg = <4>;
                                i2c-mux,deselect-on-exit;
                                #address-cells = <1>;
                                #size-cells = <0>;
                        };

                        i2c@0 {
                                tca6416_20: tca6416@20 {
                                        compatible = "ti,tca6416";
                                        gpio-controller;
                                        #gpio-cells = <2>;
                                        reg = <0x20>;
                                        vcc-supply = <&battery_reg>;
                                        tca6416_20_outhigh {
                                                /*
                                                * GPIO-0 : AVDD_CAM1
                                                * GPIO-1 : DVDD_CAM1
                                                * GPIO-2 : RST_CAM1
                                                * GPIO-3 : PWDN_CAM1
                                                * GPIO-4 : AVDD_CAM2
                                                * GPIO-5 : DVDD_CAM2
                                                * GPIO-6 : RST_CAM2
                                                * GPIO-7 : PWDN_CAM2
                                                * GPIO-8 : AVDD_CAM3
                                                * GPIO-9 : DVDD_CAM3
                                                * GPIO-10 : RST_CAM3
                                                * GPIO-11 : PWDN_CAM3
                                                * GPIO-12 : AVDD_CAM4
                                                * GPIO-13 : DVDD_CAM4
                                                * GPIO-14 : RST_CAM4
                                                * GPIO-15 : PWDN_CAM4
                                                */
                                                gpio-hog;
                                                gpios = <3 0 7 0>;
                                                output-high;
                                                label = "tca6416_20_outlow_3",
                                                        "tca6416_20_outlow_7";
						status = "okay";
                                        };
                                        tca6416_20_outlow {
                                                gpio-hog;
                                                gpios = <0 0 1 0 2 0 4 0 5 0 6 0 8 0 9 0 10 0 11 0 12 0 13 0 14 0 15 0>;
                                                output-low;
                                                label = "tca6416_20_outlow_0",
                                                        "tca6416_20_outlow_1",
                                                        "tca6416_20_outlow_2",
                                                        "tca6416_20_outlow_4",
                                                        "tca6416_20_outlow_5",
                                                        "tca6416_20_outlow_6",
                                                        "tca6416_20_outlow_8",
                                                        "tca6416_20_outlow_9",
                                                        "tca6416_20_outlow_10",
                                                        "tca6416_20_outlow_11",
                                                        "tca6416_20_outlow_12",
                                                        "tca6416_20_outlow_13",
                                                        "tca6416_20_outlow_14",
                                                        "tca6416_20_outlow_15";
                                                status = "okay";
                                        };
                                        tca6416_20_input {
                                                status = "disabled";
                                        };
                                };
                        };

                        i2c@4 {
                                ov5670_e@36 {
                                        clocks = <&tegra_car TEGRA186_CLK_EXTPERIPH1>,
                                                 <&tegra_car TEGRA186_CLK_PLLP_OUT0>;
                                        clock-names = "extperiph1", "pllp_grtba";
                                        mclk = "extperiph1";
                                        clock-frequency = <24000000>;
                                        pwdn-gpios = <&tca6416_20 15 GPIO_ACTIVE_HIGH>;
                                        reset-gpios = <&tca6416_20 14 GPIO_ACTIVE_HIGH>;
                                        vana-supply = <&en_avdd_cam4>;
                                        vdig-supply = <&en_dvdd_cam4>;
                                };
                        };

                };

	};

	gpio@2200000 {
		camera-control-output-high {
			gpio-hog;
			output-high;
			gpios = <CAM0_RST_L 0 CAM0_PWDN 0>;
			label = "cam0-rst", "cam0-pwdn";
		};
	};
};
